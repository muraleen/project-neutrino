// NEUTRINO AVR GPIO INTERFACE

#include <avr/io.h>

#ifndef GPIO_H
#define GPIO_H

#ifndef GPIO_HELPERS
#define GPIO_HELPERS
#define HIGH	0x01
#define LOW		0x00
#define OUTPUT	0x01
#define INPUT	0x00

enum pin {
	A0, A1, A2, A3, A4, A5, A6, A7, 
	B0, B1, B2, B3, B4, B5, B6, B7, 
	C0, C1, C2, C3, C4, C5, C6, C7, 
	D0, D1, D2, D3, D4, D5, D6, D7, 
	E0, E1, E2, E3, E4, E5, E6, E7, 
	F0, F1, F2, F3, F4, F5, F6, F7, 
	G0, G1, G2, G3, G4, G5, G6, G7, 
	H0, H1, H2, H3, H4, H5, H6, H7, 
	I0, I1, I2, I3, I4, I5, I6, I7, 
	J0, J1, J2, J3, J4, J5, J6, J7, 
	K0, K1, K2, K3, K4, K5, K6, K7, 
};

enum pwm {
	OC0A, OC0B, OC0C,
	OC1A, OC1B, OC1C,
	OC2A, OC2B, OC2C,
	OC3A, OC3B, OC3C,
	OC4A, OC4B, OC4C,
	OC5A, OC5B, OC5C,
};

#endif // GPIO_HELPERS

#include <stdint.h>

void gpio_initPWM();
void gpio_setDirection(int _pin, int value);
void gpio_setDigitalValue(int _pin, int value);
void gpio_setPWMValue(int _pin, float value);
int gpio_getDirection(int _pin);
int gpio_getDigitalValue(int _pin);
float gpio_getPWMSetting(int _pin);
float gpio_getAnalogValue(int _pin);
uint8_t getMask(int _pin);

#endif // GPIO_H
