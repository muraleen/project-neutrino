#include "gpio.h"

int pinDirection[88];
float pwmSetting[18];

void gpio_initPWM() {
#ifdef OCR0A
	TCCR0A |= 0b10100001;
	TCCR0B |= 0b00000011;
#endif

#ifdef OCR1A
	TCCR1A |= 0b10100001;
	TCCR1B |= 0b00000011;
#endif

#ifdef OCR2A
	TCCR2A |= 0b10100001;
	TCCR2B |= 0b00000011;
#endif

#ifdef OCR3A
	TCCR3A |= 0b10100001;
	TCCR3B |= 0b00000011;
#endif

#ifdef OCR4A
	TCCR4A |= 0b10100001;
	TCCR4B |= 0b00000011;
#endif

#ifdef OCR5A
	TCCR5A |= 0b10100001;
	TCCR5B |= 0b00000011;
#endif

}

void gpio_setDirection(int _pin, int value) {
	pinDirection[_pin] = value;
	switch((int) (_pin/8)) {
		case 0:
#ifdef DDRA
			if(value > 0) DDRA |= getMask(_pin);
			else DDRA &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 1:
#ifdef DDRB
			if(value > 0) DDRB |= getMask(_pin);
			else DDRB &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 2:
#ifdef DDRC
			if(value > 0) DDRC |= getMask(_pin);
			else DDRC &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 3:
#ifdef DDRD
			if(value > 0) DDRD |= getMask(_pin);
			else DDRD &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 4:
#ifdef DDRE
			if(value > 0) DDRE |= getMask(_pin);
			else DDRE &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 5:
#ifdef DDRF
			if(value > 0) DDRF |= getMask(_pin);
			else DDRF &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 6:
#ifdef DDRG
			if(value > 0) DDRG |= getMask(_pin);
			else DDRG &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 7:
#ifdef DDRH
			if(value > 0) DDRH |= getMask(_pin);
			else DDRH &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 8:
#ifdef DDRI
			if(value > 0) DDRI |= getMask(_pin);
			else DDRI &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 9:
#ifdef DDRJ
			if(value > 0) DDRJ |= getMask(_pin);
			else DDRJ &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 10:
#ifdef DDRK
			if(value > 0) DDRK |= getMask(_pin);
			else DDRK &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 11:
#ifdef DDRL
			if(value > 0) DDRL |= getMask(_pin);
			else DDRL &= ~getMask(_pin);
			break;
#else
			return;
#endif
	}
}

void gpio_setDigitalValue(int _pin, int value) {
	switch((int) (_pin/8)) {
		case 0:
#ifdef PORTA
			if(value > 0) PORTA |= getMask(_pin);
			else PORTA &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 1:
#ifdef PORTB
			if(value > 0) PORTB |= getMask(_pin);
			else PORTB &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 2:
#ifdef PORTC
			if(value > 0) PORTC |= getMask(_pin);
			else PORTC &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 3:
#ifdef PORTD
			if(value > 0) PORTD |= getMask(_pin);
			else PORTD &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 4:
#ifdef PORTE
			if(value > 0) PORTE |= getMask(_pin);
			else PORTE &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 5:
#ifdef PORTF
			if(value > 0) PORTF |= getMask(_pin);
			else PORTF &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 6:
#ifdef PORTG
			if(value > 0) PORTG |= getMask(_pin);
			else PORTG &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 7:
#ifdef PORTH
			if(value > 0) PORTH |= getMask(_pin);
			else PORTH &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 8:
#ifdef PORTI
			if(value > 0) PORTI |= getMask(_pin);
			else PORTI &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 9:
#ifdef PORTJ
			if(value > 0) PORTJ |= getMask(_pin);
			else PORTJ &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 10:
#ifdef PORTK
			if(value > 0) PORTK |= getMask(_pin);
			else PORTK &= ~getMask(_pin);
			break;
#else
			return;
#endif
		case 11:
#ifdef PORTL
			if(value > 0) PORTL |= getMask(_pin);
			else PORTL &= ~getMask(_pin);
			break;
#else
			return;
#endif
	}
}

void gpio_setPWMValue(int _pin, float value) {
	pwmSetting[_pin] = value;
	switch(_pin) {
#ifdef OCR0A
		case OC0A:
			OCR0A = (int) (value*255);
			break;
		case OC0B:
			OCR0B = (int) (value*255);
			break;
#endif

#ifdef OCR0C
		case OC0C:
			OCR0C = (int) (value*255);
			break;
#endif

#ifdef OCR1A
		case OC1A:
			OCR1A = (int) (value*255);
			break;
		case OC1B:
			OCR1B = (int) (value*255);
			break;
#endif

#ifdef OCR1C
		case OC1C:
			OCR1C = (int) (value*255);
			break;
#endif

#ifdef OCR2A
		case OC2A:
			OCR2A = (int) (value*255);
			break;
		case OC2B:
			OCR2B = (int) (value*255);
			break;
#endif

#ifdef OCR2C
		case OC2C:
			OCR2C = (int) (value*255);
			break;
#endif

#ifdef OCR3A
		case OC3A:
			OCR3A = (int) (value*255);
			break;
		case OC3B:
			OCR3B = (int) (value*255);
			break;
#endif

#ifdef OCR3C
		case OC3C:
			OCR3C = (int) (value*255);
			break;
#endif

#ifdef OCR4A
		case OC4A:
			OCR4A = (int) (value*255);
			break;
		case OC4B:
			OCR4B = (int) (value*255);
			break;
#endif

#ifdef OCR4C
		case OC4C:
			OCR4C = (int) (value*255);
			break;
#endif

#ifdef OCR5A
		case OC5A:
			OCR5A = (int) (value*255);
			break;
		case OC5B:
			OCR5B = (int) (value*255);
			break;
#endif

#ifdef OCR5C
		case OC5C:
			OCR5C = (int) (value*255);
			break;
#endif

		default:
			return;
	}
}

int gpio_getDirection(int _pin) {
	return pinDirection[_pin];
}

float gpio_getPWMSetting(int _pin) {
	return pwmSetting[_pin];
}

int gpio_getDigitalValue(int _pin) {
	switch((int) (_pin/8)) {
		case 0:
#ifdef PINA
			if((PINA & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 1:
#ifdef PINB
			if((PINB & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 2:
#ifdef PINC
			if((PINC & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 3:
#ifdef PIND
			if((PIND & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 4:
#ifdef PINE
			if((PINE & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 5:
#ifdef PINF
			if((PINF & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 6:
#ifdef PING
			if((PING & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 7:
#ifdef PINH
			if((PINH & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 8:
#ifdef PINI
			if((PINI & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 9:
#ifdef PINJ
			if((PINJ & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 10:
#ifdef PINK
			if((PINK & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
		case 11:
#ifdef PINL
			if((PINL & getMask(_pin)) > 0) return 1;
			else return 0;
#else
			return -1;
#endif
	}
}

float gpio_getAnalogValue(int _pin) {
	// FIXME - Find out how to do this!!!
}

uint8_t getMask(int _pin) {
	uint8_t pByte = 0x01 << _pin%8;
	return pByte;
}
