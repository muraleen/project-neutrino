// AVR COMPATIBILITY - Narendran Muraleedharan <muraleen@my.erau.edu>

#ifndef COMPAT_H
#define COMPAT_H

#include "freq.h"
#include <util/delay.h>

#include "clock/clock.h"
#include "gpio/gpio.h"
#include "uart/uart.h"

#endif // COMPAT_H
