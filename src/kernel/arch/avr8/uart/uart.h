// UART COMPATIBILITY FOR AVR - Narendran Muraleedharan <muraleen@my.erau.edu>

#ifndef UART_H
#define UART_H

#include "../freq.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdint.h>

#define BUFFER_SIZE 128

// UARTx_STS: Status
// UARTx_CTL: Control
// UARTx_DAT: Data
// UARTx_EMP: Empty
// UARTx_RXE: Rx Enabled
// UARTx_TXE: Tx Enabled
// UARTx_RCI: Rx Complete Interrup
// UARTx_RXC: Rx Complete Flag

#ifdef UCSRA // UART0
#define UART0
#define UART0_STS	UCSRA
#define UART0_CTL	UCSRB
#define UART0_DAT	UDR
#define UART0_EMP	UDRIE
#define UART0_UBRRL	UBRRL
#define UART0_UBRRH	UBRRH
#define UART0_RXE	RXEN
#define UART0_TXE	TXEN
#define UART0_RCI	RXCIE
#define UART0_RXC	RXC
#define UART0_UDRE	UDRE
#else // UART0 for some other processors
#define UART0
#define UART0_STS	UCSR0A
#define UART0_CTL	UCSR0B
#define UART0_DAT	UDR0
#define UART0_EMP	UDRIE0
#define UART0_UBRRL	UBRR0L
#define UART0_UBRRH	UBRR0H
#define UART0_RXE	RXEN0
#define UART0_TXE	TXEN0
#define UART0_RCI	RXCIE0
#define UART0_RXC	RXC0
#define UART0_UDRE	UDRE0
#endif

// UART0 Receive Interrupt Vector
#ifdef UART_RX_vect
#define UART0_RXI	UART_RX_vect
#elif defined UART_RXC_vect
#define UART0_RXI	UART_RXC_vect
#elif defined UART0_RX_vect
#define UART0_RXI	UART0_RX_vect
#elif defined UART0_RXC_vect
#define UART0_RXI	UART0_RXC_vect
#elif defined USART_RX_vect
#define UART0_RXI	USART_RX_vect
#elif defined USART_RXC_vect
#define UART0_RXI	USART_RXC_vect
#elif defined USART0_RX_vect
#define UART0_RXI	USART0_RX_vect
#elif defined USART0_RXC_vect
#define UART0_RXI	USART0_RXC_vect
#endif

// UART0 Transmit Interrupt Vector
#ifdef UART_UDRE_vect
#define UART0_TXI	UART_UDRE_vect
#elif defined UART0_UDRE_vect
#define UART0_TXI	UART0_UDRE_vect
#elif defined USART_UDRE_vect
#define UART0_TXI	USART_UDRE_vect
#elif defined USART0_UDRE_vect
#define UART0_TXI	USART0_UDRE_vect
#endif

#ifdef UCSR1A // UART1
#define UART1
#define UART1_STS	UCSR1A
#define UART1_CTL	UCSR1B
#define UART1_DAT	UDR1
#define UART1_EMP	UDRIE1
#define UART1_UBRRL	UBRR1L
#define UART1_UBRRH	UBRR1H
#define UART1_RXE	RXEN1
#define UART1_TXE	TXEN1
#define UART1_RCI	RXCIE1
#define UART1_RXI	USART1_RX_vect
#define UART1_TXI	USART1_UDRE_vect
#define UART1_RXC	RXC1
#define UART1_UDRE	UDRE1
#endif

#ifdef UCSR2A // UART2
#define UART2
#define UART2_STS	UCSR2A
#define UART2_CTL	UCSR2B
#define UART2_DAT	UDR2
#define UART2_EMP	UDRIE2
#define UART2_UBRRL	UBRR2L
#define UART2_UBRRH	UBRR2H
#define UART2_RXE	RXEN2
#define UART2_TXE	TXEN2
#define UART2_RCI	RXCIE2
#define UART2_RXI	USART2_RX_vect
#define UART2_TXI	USART2_UDRE_vect
#define UART2_RXC	RXC2
#define UART2_UDRE	UDRE2
#endif

#ifdef UCSR3A // UART3
#define UART3
#define UART3_STS	UCSR3A
#define UART3_CTL	UCSR3B
#define UART3_DAT	UDR3
#define UART3_EMP	UDRIE3
#define UART3_UBRRL	UBRR3L
#define UART3_UBRRH	UBRR3H
#define UART3_RXE	RXEN3
#define UART3_TXE	TXEN3
#define UART3_RCI	RXCIE3
#define UART3_RXI	USART3_RX_vect
#define UART3_TXI	USART3_UDRE_vect
#define UART3_RXC	RXC3
#define UART3_UDRE	UDRE3
#endif

#ifdef __cplusplus
extern "C"{
	// static FILE UARTstdout = FDEV_SETUP_STREAM(uart0_putc, NULL, _FDEV_SETUP_WRITE);
	static FILE * UARTstream;
}
#endif

typedef struct{
	uint8_t buffer[BUFFER_SIZE];
	uint8_t rN; // Read Index
	uint8_t wN; // Write Index
	bool ready;
}uartBuf;

int uart0_putc(char c, FILE *stream);
void writeToBuffer(uartBuf *buf, uint8_t data);
char readFromBuffer(uartBuf *buf);

class Uart {
	public:
		Uart(int _id);
		void init(uint32_t _baud);
		void put(char data);
		void putStr(char *s);
		char get(void);
		bool isReady(void);
		
	private:
		int id;
};

#endif // UART_H
