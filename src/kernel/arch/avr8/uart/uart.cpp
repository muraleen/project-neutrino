#include "uart.h"

uartBuf uart0Buf;

#ifdef UART1
uartBuf uart1Buf;
#endif

#ifdef UART2
uartBuf uart2Buf;
#endif

#ifdef UART3
uartBuf uart3Buf;
#endif

Uart::Uart(int _id) {
	id = _id;
}

void Uart::init(uint32_t _baud) {
	uint16_t baud_set = (F_CPU/4/_baud - 1)/2;
	switch(id) {
#ifdef UART1
		case 1:
			UART1_STS = (1<<U2X1);
			if(((F_CPU == 16000000UL) && (_baud == 57600)) || (baud_set >4095)) {
				UART1_STS = 0;
				baud_set = (F_CPU/8/_baud - 1)/2;
			}
			UART1_UBRRL = (uint8_t) baud_set;
			UART1_UBRRH = (uint8_t) (baud_set >> 8);
			UART1_CTL = _BV(UART1_RCI) | (1 << UART1_RXE) | (1 << UART1_TXE);
#ifdef URSEL1
			UCSR1C = (1<<URSEL1) | (1<<UCSZ10);
#else
			UCSR1C = (1<<UCSZ10);
#endif
			uart1Buf.rN = 0;
			uart1Buf.wN = 0;
			uart1Buf.ready = false;
			break;
#endif
#ifdef UART2
		case 2:
			UART2_STS = (1<<U2X2);
			if(((F_CPU == 16000000UL) && (_baud == 57600)) || (baud_set >4095)) {
				UART2_STS = 0;
				baud_set = (F_CPU/8/_baud - 1)/2;
			}
			UART2_UBRRL = (uint8_t) baud_set;
			UART2_UBRRH = (uint8_t) (baud_set >> 8);
			UART2_CTL = _BV(UART2_RCI) | (1 << UART2_RXE) | (1 << UART2_TXE);
#ifdef URSEL2
			UCSR2C = (1<<URSEL2) | (1<<UCSZ20);
#else
			UCSR2C = (1<<UCSZ20);
#endif
			uart2Buf.rN = 0;
			uart2Buf.wN = 0;
			uart2Buf.ready = false;
			break;
#endif
#ifdef UART3
		case 3:
			UART3_STS = (1<<U2X3);
			if(((F_CPU == 16000000UL) && (_baud == 57600)) || (baud_set >4095)) {
				UART3_STS = 0;
				baud_set = (F_CPU/8/_baud - 1)/2;
			}
			UART3_UBRRL = (uint8_t) baud_set;
			UART3_UBRRH = (uint8_t) (baud_set >> 8);
			UART3_CTL = _BV(UART3_RCI) | (1 << UART3_RXE) | (1 << UART3_TXE);
#ifdef URSEL3
			UCSR3C = (1<<URSEL3) | (1<<UCSZ30);
#else
			UCSR3C = (1<<UCSZ30);
#endif
			uart3Buf.rN = 0;
			uart3Buf.wN = 0;
			uart3Buf.ready = false;
			break;
#endif
		default:
			id = 0;
			// Initialize UART0 by default to prevent any crashes		   FIXME
			UART0_STS = (1<<U2X0);
			if(((F_CPU == 16000000UL) && (_baud == 57600)) || (baud_set >4095)) {
				UART0_STS = 0;
				baud_set = (F_CPU/8/_baud - 1)/2;
			}
			UART0_UBRRL = (uint8_t) baud_set;
			UART0_UBRRH = (uint8_t) (baud_set >> 8);
			UART0_CTL |= (1 << UART0_RCI) | (1 << UART0_RXE) | (1 << UART0_TXE);
#ifdef UCSZ0
#ifdef URSEL
			UCSRC |= (1<<URSEL) | (1<<UCSZ0) | (1<<UCSZ1);
#else
			UCSRC |= (1<<USBS) | (1<<UCSZ0);
#endif
#else
#ifdef URSEL0
			UCSR0C |= (1<<URSEL0) | (1<<UCSZ00) | (1<<UCSZ01);
#else
			UCSR0C |= (1<<UCSZ00) | (1<<USBS0);
#endif
#endif
			uart0Buf.rN = 0;
			uart0Buf.wN = 0;
			uart0Buf.ready = false;
			
			// For some reason, this doesn't work anymore...			   FIXME
			// I have a temporary workaround, but this still needs to be fixed!			
			
			UARTstream = fdevopen(uart0_putc, NULL);
			stdout = UARTstream;
	}
}

int uart0_putc(char c, FILE *stream) {
	if(c == '\n') uart0_putc('\r', stream);
	loop_until_bit_is_set(UART0_STS, UART0_UDRE);
	UART0_DAT = c;
	
	return 0;
}

void Uart::put(char data) {
	switch(id) {
		case 0:
			while(!(UART0_STS & (1<<UART0_UDRE))); // Wait for empty Tx buffer
			UART0_DAT = data;
			break;
			
#ifdef UART1
		case 1:
			while(!(UART1_STS & (1<<UART1_UDRE))); // Wait for empty Tx buffer
			UART1_DAT = data;
			break;
#endif

#ifdef UART2
		case 2:
			while(!(UART2_STS & (1<<UART2_UDRE))); // Wait for empty Tx buffer
			UART2_DAT = data;
			break;
#endif

#ifdef UART3
		case 3:
			while(!(UART3_STS & (1<<UART3_UDRE))); // Wait for empty Tx buffer
			UART3_DAT = data;
			break;
#endif
	}
}

bool Uart::isReady(void) {
	switch(id) {
		case 0:
			if(uart0Buf.ready == true) return true;
			else return false;
						
#ifdef UART1
		case 1:
			if(uart1Buf.ready == true) return true;
			else return false;
#endif

#ifdef UART2
		case 2:
			if(uart2Buf.ready == true) return true;
			else return false;
#endif

#ifdef UART3
		case 3:
			if(uart3Buf.ready == true) return true;
			else return false;
#endif
	}
}

void Uart::putStr(char *s) {
	while(*s) put(*s++);
}

char Uart::get(void) {
	switch(id) {
		case 0:
			return readFromBuffer(&uart0Buf);
			
#ifdef UART1
		case 1:
			return readFromBuffer(&uart1Buf);
#endif

#ifdef UART2
		case 2:
			return readFromBuffer(&uart2Buf);
#endif

#ifdef UART3
		case 3:
			return readFromBuffer(&uart3Buf);
#endif
	}
}

void writeToBuffer(uartBuf *buf, uint8_t data) {
	if(buf->wN >= BUFFER_SIZE) buf->wN = 0;
	buf->buffer[buf->wN] = data;
	buf->wN++;
	buf->ready = true;
}

char readFromBuffer(uartBuf *buf) {
	if((buf->wN > buf->rN) || (buf->wN < (buf->rN - (BUFFER_SIZE/2)))) {
		if(buf->rN >= BUFFER_SIZE) buf->rN = 0;
		char data = buf->buffer[buf->rN];
		buf->rN++;
		if(buf->rN >= buf->wN) buf->ready = false;
		return data;
	} else {
		buf->ready = false;
		return 0;
	}
}

ISR(UART0_RXI) {
	while(!(UART0_STS & (1 << UART0_RXC)));
	writeToBuffer(&uart0Buf, UART0_DAT);
}

#ifdef UART1
ISR(UART1_RXI) {
	while(!(UART1_STS & (1 << UART1_RXC)));
	writeToBuffer(&uart1Buf, UART1_DAT);
}
#endif

#ifdef UART2
ISR(UART2_RXI) {
	while(!(UART2_STS & (1 << UART2_RXC)));
	writeToBuffer(&uart2Buf, UART2_DAT);
}
#endif

#ifdef UART3
ISR(UART3_RXI) {
	while(!(UART3_STS & (1 << UART3_RXC)));
	writeToBuffer(&uart3Buf, UART3_DAT);
}
#endif
