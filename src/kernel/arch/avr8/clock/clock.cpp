#include "clock.h"

volatile unsigned long timer1_ms;

unsigned long millis(void) {
	unsigned long MS;
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		MS = timer1_ms;
	}
	return MS;
}

void initClk(void) {
	// SET UP CLOCK
	TCCR1B |= (1 << WGM12) | (1 << CS11); // CTC mode: Clock/8
	OCR1AH = (CTC_MATCH_OVERFLOW >> 8);
	OCR1AL = (unsigned char) CTC_MATCH_OVERFLOW;
	TIMSK1 |= (1 << OCIE1A); // Enable compare match interrupt	
}

ISR(TIMER1_COMPA_vect) {
	timer1_ms++;
}
