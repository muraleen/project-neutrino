#ifndef CLOCK_H
#define CLOCK_H

#include "../freq.h"

#define CTC_MATCH_OVERFLOW ((F_CPU / 1000) / 8)

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

unsigned long millis(void); // Returns miliiseconds passed
void initClk(void); // Initialize clock

#endif // CLOCK_H
