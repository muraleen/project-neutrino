// NEUTRINO TASK SCHEDULER - Narendran Muraleedharan <muraleen@my.erau.edu>
// Based on Brad Luyster's AVRQueue library: https://github.com/Zuph/AVRQueue

#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "task.h"

// Increase QUEUE_LEN to add more tasks to queue
#define QUEUE_LEN 32

class Scheduler {
	public:
		Scheduler(); // CONSTRUCTOR
		int addTask(qFunc func, const char *id, unsigned long initT, unsigned long recur);
		int rmTask(const char *id);
		int chTask(const char *id, unsigned long nextT, unsigned long recur);
		
		int run(unsigned long now);
	
	private:
		unsigned int _qStart;
		unsigned int _qEnd;
		unsigned int _tasks;
		Task _schedule[QUEUE_LEN];
		
		int _getNextTask(Task &task);
		int _addToQueue(Task &task);
		
};

#endif // SCHEDULER_H
