// NEUTRINO TASK STRUCTURE - Narendran Muraleedharan <muraleen@my.erau.edu>

#ifndef TASK_H
#define TASK_H

typedef int (*qFunc)(unsigned long);

struct Task {
	qFunc func;
	char name[8];
	unsigned long next;
	unsigned long recur;
};

#endif // TASK_H
