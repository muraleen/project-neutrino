// NEUTRINO TASK SCHEDULER - Narendran Muraleedharan <muraleen@my.erau.edu>
// Based on Brad Luyster's AVRQueue library: https://github.com/Zuph/AVRQueue

#include "scheduler.h"
#include <string.h>

Scheduler::Scheduler() {
	_qStart = 0;
	_qEnd = 0;
	_tasks = 0;
}

int Scheduler::addTask(qFunc func, const char *id, unsigned long initT, unsigned long recur) {
	if (strlen(id) > 7)
		return -1;
	else {
		Task m;
		m.func = func; // Point to function
		memset(m.name, 0, 8);
		memcpy(m.name, id, strlen(id));
		m.recur = recur;
		m.next = initT;
		
		return _addToQueue(m);
	}
}

int Scheduler::rmTask(const char *id) {
	Task target;
	int rCode = -1;
	for(int i=0; i<_tasks; ++i) {
		if(_getNextTask(target) == 0) {
			if(strcmp(target.name, id) == 0) {
				rCode = 0;
			} else {
				_addToQueue(target);
			}
		} else {
			rCode = -1;
			break;
		}
	}
	
	return rCode;
}

int Scheduler::chTask(const char *id, unsigned long nextT, unsigned long recur) {
	Task target;
	int rCode = -1;
	for(int i=0; i<_tasks; ++i) {
		if(_getNextTask(target) == 0) {
			if(strcmp(target.name, id) == 0) {
				target.next = nextT;
				target.recur = recur;
				rCode = 0;
			}
			_addToQueue(target);
		} else {
			rCode = -1;
			break;
		}
	}
	
	return rCode;
}

int Scheduler::run(unsigned long now) {
	Task target;
	int rCode = 0;
	if(_tasks == 0)
		rCode = -1;
	
	for(int i=0; i<_tasks; ++i) {
		if(_getNextTask(target) == 0) {
			if(target.next <= now)
			{
				int tRv;
				tRv = (target.func)(now);
				if(tRv == 0) {
					rCode++;
				}
				if(target.recur != 0) {
					target.next = now + target.recur;
					_addToQueue(target);
				}
			} else {
				_addToQueue(target);
			}
		} else {
			rCode = -1;
			break;
		}
	}
	
	return rCode;
}

int Scheduler::_getNextTask(Task &task) {
	int rCode = 0;
	if(_qEnd != _qStart) {
		Task tmp = _schedule[_qStart];
		_qStart = (_qStart + 1) % QUEUE_LEN;
		task = tmp;
		_tasks--;
	} else {
		rCode = -1;
	}
	
	return rCode;
}

int Scheduler::_addToQueue(Task &task) {
	int rCode = 0;
	if((_qEnd + 1) % QUEUE_LEN != _qStart) {
		_schedule[_qEnd] = task;
		_qEnd = (_qEnd + 1) % QUEUE_LEN;
		_tasks++;
	} else {
		rCode = -1;
	}
	
	return rCode;
}
