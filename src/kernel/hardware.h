// Include Hardware Co

#ifndef HARDWARE_H
#define HARDWARE_H

#ifdef ARCH_AVR8
#include "arch/avr8/compat.h"
#endif

#ifdef ARCH_AVR32
#include "arch/avr32/compat.h"
#endif

#ifdef ARCH_ARM
#include "arch/armv7/compat.h"
#endif

#endif // HARDWARE_H
