#ifndef CONSOLE_H
#define CONSOLE_H

#include "../hardware.h"

class Console {
	public:
		Console(Uart *_stream);
		void run(void); // SCHEDULER TASK
		// Console Manipulation Commands
		void clear(int init);
		void print(char *s);
		void println(char *s);
	
	private:
		Uart *stream;
		char cmd[256];
		bool esc1;
		bool esc2;
		void handleInput(char c);
		void runCmd();
		
};

#endif // CONSOLE_H
