#include "console.h"
#include <string.h>
#include <stdio.h>

#include "../../modules/modules.h"

Console::Console(Uart *_stream) {
	stream = _stream;
}

void Console::run(void) {
	while(stream->isReady()) handleInput(stream->get());
}

void Console::handleInput(char c) {
	if(c == 0x1B) c = 0x5E;
	switch(c) {
		// Handle Escape characters, backspce, return etc.
		case 0x08: // Backspace
			if(strlen(cmd) > 0) {
				stream->putStr("\b \b");
				size_t len = strlen(cmd);
				cmd[len-1] = '\0';
			}
			break;
		case 0x0D: // Carraige Return
			stream->putStr("\n\r");
			runCmd();
			cmd[0] = '\0';
			stream->putStr(">> ");
			break;
		// Handle other characters (basically, just append to the cmd variable)
		default:
			stream->put(c);
			char _c[2] = {c, '\0'};
			strcat(cmd, _c);		
	}
}

void Console::runCmd(void) {
	v_parseCmd(this, cmd);
}

void Console::clear(int init) {
	stream->put(0x1B); // ESCAPE
	stream->putStr("[2J");
	cmd[0] = '\0';
	if(init) stream->putStr(">> ");
}

void Console::print(char *s) {
	stream->putStr(s);
}

void Console::println(char *s) {
	stream->putStr(s);
	stream->putStr("\n\r");
}
