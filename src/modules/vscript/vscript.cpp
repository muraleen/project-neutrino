/* vScript commands: ***********************************************************

> gpio.<pin_ID>.[direction|value] { = arg}
> uart.<uart_ID>.[baud|write|read|waitForChar|readUntilChar|readNChar] {args}
> run {app_name}

*******************************************************************************/

#include "vscript.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/* enum pin {
	A0, A1, A2, A3, A4, A5, A6, A7, 
	B0, B1, B2, B3, B4, B5, B6, B7, 
	C0, C1, C2, C3, C4, C5, C6, C7, 
	D0, D1, D2, D3, D4, D5, D6, D7, 
	E0, E1, E2, E3, E4, E5, E6, E7, 
	F0, F1, F2, F3, F4, F5, F6, F7, 
	G0, G1, G2, G3, G4, G5, G6, G7, 
	H0, H1, H2, H3, H4, H5, H6, H7, 
	I0, I1, I2, I3, I4, I5, I6, I7, 
	J0, J1, J2, J3, J4, J5, J6, J7, 
	K0, K1, K2, K3, K4, K5, K6, K7, 
}; */

const char pinMap[88][3] = {"A0", "A1", "A2", "A3", "A4", "A5", "A6", "A6",
							"B0", "B1", "B2", "B3", "B4", "B5", "B6", "B6",
							"C0", "C1", "C2", "C3", "C4", "C5", "C6", "C6",
							"D0", "D1", "D2", "D3", "D4", "D5", "D6", "D6",
							"E0", "E1", "E2", "E3", "E4", "E5", "E6", "E6",
							"F0", "F1", "F2", "F3", "F4", "F5", "F6", "F6",
							"G0", "G1", "G2", "G3", "G4", "G5", "G6", "G6",
							"H0", "H1", "H2", "H3", "H4", "H5", "H6", "H6",
							"I0", "I1", "I2", "I3", "I4", "I5", "I6", "I6",
							"J0", "J1", "J2", "J3", "J4", "J5", "J6", "J6",
							"K0", "K1", "K2", "K3", "K4", "K5", "K6", "K7"};

/* enum pwm {
	OC0A, OC0B, OC0C,
	OC1A, OC1B, OC1C,
	OC2A, OC2B, OC2C,
	OC3A, OC3B, OC3C,
	OC4A, OC4B, OC4C,
	OC5A, OC5B, OC5C,
}; */

const char pwmMap[18][5] = {"OC0A", "OC0B", "OC0C",
							"OC1A", "OC1B", "OC1C",
							"OC2A", "OC2B", "OC2C",
							"OC3A", "OC3B", "OC3C",
							"OC4A", "OC4B", "OC4C",
							"OC5A", "OC5B", "OC5C"};

enum dictID {
	PINMAP,
	PWMMAP
};

int getIndex(int DICT_ID, char *str) {
	switch(DICT_ID) {
		case PINMAP:
			for(int i=0; i<88; i++) if(!strcmp(pinMap[i],str)) return i;
			break;
		case PWMMAP:
			for(int i=0; i<18; i++) if(!strcmp(pwmMap[i],str)) return i;
			break;
	}
	return -1; // NOT FOUND!
}

void v_parseCmd(Console *console, char *_cmd) {
	if(!strcmp(_cmd, "clear")) {
		console->clear(0);
		return;
	} else {
		char *end_str;
		char *token = strtok_r(_cmd, " ", &end_str);
		int i=0;
		while(token != NULL) {		
			if(i == 0) { // Function/Variable pointer
				char *end_token;
				char *token2 = strtok_r(token, ".", &end_token);
				int j=0;
				while(token2 != NULL) {
					if(j == 0) {
						// PARAM1
						if(!strcmp(token2,"gpio")) {
							// Next token
							token2 = strtok_r(NULL, ".", &end_token); j++;
							// PARAM2 - pinID
							int pwmMode = 0;
							int pinN = getIndex(PINMAP, token2);
							if(pinN < 0) { 
								pinN = getIndex(PWMMAP, token2);
								pwmMode = 1;
							}
							if(pinN < 0) {
								console->print("Error: [vScript:GPIO] pin doesn't exist: ");
								console->println(token2);
								return;
							}
							
							// Got so far, that means the pin exists
							// Next token should be either direction or value
							token2 = strtok_r(NULL, ".", &end_token); j++;
							if(!strcmp(token2, "direction")) {
								token = strtok_r(NULL, " ", &end_str); i++;
								if(!strcmp(token,"<")) {
									token = strtok_r(NULL, " ", &end_str); i++;
									// SET GPIO DIRECTION
									if(!strcmp(token,"1") || !strcmp(token,"OUTPUT")) {
										gpio_setDirection(pinN, 1);
										return;
									} else {
										gpio_setDirection(pinN, 0);
										return;
									}
									return;
								} else {
									// PRINT GPIO DIRECTION
									if(gpio_getDirection(pinN)) console->println("OUTPUT");
									else console->println("INPUT");
									return;
								}
							} else if(!strcmp(token2, "value")) {
								token = strtok_r(NULL, " ", &end_str); i++;
								if(!strcmp(token,"<")) {
									token = strtok_r(NULL, " ", &end_str); i++;
									// SET GPIO VALUE
									if(pwmMode) {
										gpio_setPWMValue(pinN, atof(token));
										return;
									} else {
										if(!strcmp(token,"1") || !strcmp(token,"HIGH")) {
											gpio_setDigitalValue(pinN, 1);
											return;
										} else {
											gpio_setDigitalValue(pinN, 0);
											return;
										}
									}
									return;
								} else {
									// PRINT GPIO VALUE
									if(pwmMode) {
										char str[4];
										sprintf(str,"%1.2f", gpio_getPWMSetting(pinN));
										console->println(str);
										return;
									} else {
										if(gpio_getDigitalValue(pinN)) console->println("HIGH");
										else console->println("LOW");
										return;
									}
								}
							} else {
								console->print("Error: [vScript:GPIO] property doesn't exist: ");
								console->println(token2);
								return;
							}
						}
					}
				
					token2 = strtok_r(NULL, ".", &end_token); j++;
				}
			}
			token = strtok_r(NULL, " ", &end_str); i++;
		}
	}
	console->println("Error: [vScript] command not found"); return;
}
