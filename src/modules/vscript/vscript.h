// NEUTRINO SCRIPT INTERPRETER - Narendran Muraleedharan <muraleen@my.erau.edu>

#ifndef VSCRIPT_H
#define VSCRIPT_H

#include "../../kernel/kernel.h"

void v_parseCmd(Console *console, char *_cmd);
int getIndex(int DICT_ID, char *str);

#endif // VSCRIPT_H
