#include "kernel/kernel.h"
#include "modules/modules.h"

Uart uart0(0);
Console console(&uart0);
Scheduler scheduler;

int task_console(unsigned long now) { console.run(); };
int ledOn(unsigned long now) { gpio_setDigitalValue(B5, 1); };
int ledOff(unsigned long now) { gpio_setDigitalValue(B5, 0); };

int main(void)
{
	initClk();
	uart0.init(115200);
	console.clear(1);
	gpio_initPWM(); // Required for vScript PWM write
	
	gpio_setDirection(B5, 1);
	
	sei(); // Set External Interrups

	// Set up scheduler: add OS tasks
	scheduler.addTask(task_console, "console", 0, 20);
	scheduler.addTask(ledOn, "ledOn", 1000, 1000);
	scheduler.addTask(ledOff, "ledOff", 1100, 1000);

	while(1) scheduler.run(millis());

	return 0;
}
