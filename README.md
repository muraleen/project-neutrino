# README #

### Project Neutrino ###

The project aims at developing a lightweight and low-power-consumption device-side operating system for IoT applications. The idea behind neutrino is being able to connect simple devices like AVR/ARM development boards and Arduinos to the internet, access GPIOs and other peripherals over the internet on command and be able to install and run multiple "apps" and "scripts" without a physical connection.

### Build Instructions ###

* Clone project repository
```
#!sh

git clone git@bitbucket.org:muraleen/project-neutrino.git

```

* Install Appropriate C/C++ GNU Tool-chain and Build Dependencies
AVR:
```
#!sh

sudo apt-get install flex byacc bison gcc libusb-dev libc6-dev gcc-avr avr-libc avrdude

```

ARM (make sure you also have [OpenOCD](http://openocd.sourceforge.net/)):
```
#!sh

sudo apt-get install binutils-arm-none-eabi gdb-arm-none-eabi gcc-arm-none-eabi libnewlib-arm-none-eabi 

```
Check out [this article](http://azug.minpet.unibas.ch/~lukas/bricol/ti_simplelink/CC3200-LaunchXL.html) to set up a TI CC3200 SimpleLink Wifi Module for development.

* Build project for required architecture and upload to MCU

```
#!sh

cd project-neutrino
make ARCH=<arch>

```

Set <arch> to the MCU's architecture (the flags are down in the supported architectures section). By default, the target architecture is set to AVR 8-bit. You can set target to 'hex' to only build the binary and hex file, 'upload' to upload a previously built hex file (project-neutrino/hex/neutrino.hex) to an MCU or not set a target to do both.

Example:

```
#!sh

make ARCH=armv7

```

* Update project from git

```
#!sh

git pull git@bitbucket.org:muraleen/project-neutrino.git

```
### Supported Architectures ###

* AVR 8bit [ARCH=avr8]
* AVR 32bit [ARCH=avr32] - Work in Progress
* ARM v7 [ARCH=armv7] - Work in Progress

### TO-DO ###

* Set up a GPIO ADC interface (read analog voltage)
* Write a .vScript (Neutrino Script) interpreter
* Build a .vApp (Neutrino Application) execution and interface system
* [ARM Cortex-M4 Compatibility and Linux Development Env](https://hackpad.com/Using-the-CC3200-Launchpad-Under-Linux-Rrol11xo7NQ)

### Project Owner ###

* Narendran Muraleedharan - <muraleen@my.erau.edu>