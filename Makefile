ifeq ($(ARCH),avr8)
	include Makefile.avr8
endif

ifeq ($(ARCH),avr32)
	include Makefile.avr32
endif

ifeq ($(ARCH),arm)
	include Makefile.arm
endif

ifndef ARCH
	include Makefile.avr8
endif
